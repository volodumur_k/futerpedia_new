(function() {

      var speedModifier = 4; // parallax speed modifier

      window.onscroll = function() {

        document.querySelector(".section-Kostenloser").style.backgroundPosition = "0px " + ( window.pageYOffset / speedModifier ) + "px";

      }
 })();

function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#fileupload").change(function() {
  readURL(this);
});

